#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include "parser.h"
extern FILE* yyin;
FILE *f;
FILE *r;

void cleanup(int exit_int) {
    fclose(f);
    fclose(r);
    remove("new");
    printf("\n");
    exit(exit_int);
}

int main() {
    signal(SIGINT, cleanup);
    r = fopen("new", "w");
    fflush(r);
    f = fopen("new", "r");
    yyin = f;
    char buffer[256];
    do {
        printf("enter an expression\n");
        fgets(buffer, 256, stdin);
        fprintf(r, "%s", buffer);
        fflush(r);

        printf("you entered: %s", buffer);
    } while (yyparse());
    printf("exiting...\n");
    cleanup(0);
}
