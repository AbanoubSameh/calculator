%{
#include <stdio.h>
#include <stdlib.h>
#define YYSTYPE double
#include "parser.h"
void cleanup(int);
%}

%%

[0-9]+              { yylval = atoi(yytext); return NUM; }
[0-9]*\.[0-9]+      { yylval = atof(yytext); return NUM; }
0[xX][0-9a-fA-F]+   { yylval = (int) strtol(yytext, NULL, 0); return NUM; }
[0-9a-fA-F]+[Hh]    { yylval = (int) strtol(yytext, NULL, 16); return NUM; }
0[bB][01]+          { yylval = (int) strtol(yytext+2, NULL, 2); return NUM; }
[01]+[Bb]           { yylval = (int) strtol(yytext, NULL, 2); return NUM; }
\+                  { return ADD; }
\-                  { return SUB; }
\*                  { return MUL; }
\/                  { return DIV; }
\%                  { return MOD; }
[\n]                { return END; }
\(                  { return OPR; }
\)                  { return CPR; }
"sin("              { return SIN; }
"cos("              { return COS; }
"tan("              { return TAN; }
"asin("             { return ASIN; }
"acos("             { return ACOS; }
"atan("             { return ATAN; }
"PI"|"pi"           { return PI; }
"E"|"e"             { return E; }
"**"                { return POW; }
"log"               { return LOG; }
"ln"                { return LN; }

\&\&                { return AND; }
\|\|                { return OR; }
\&                  { return BAND; }
\|                  { return BOR; }
\^                  { return BXOR; }

\=\=                { return EQU; }
\!\=                { return NEQ; }
\<                  { return STH; }
\>                  { return GTH; }
\<\=                { return SEQ; }
\>\=                { return GEQ; }

\<\<                { return SHL; }
\>\>                { return SHR; }
\<\<\<              { return ROL; }
\>\>\>              { return ROR; }

\~                  { return TIL; }
\!                  { return NOT; }

":BIN"|":bin"       { return BIN; }
":HEX"|":hex"       { return HEX; }

[ \t]               { /* do nothing */ }
[^0-9]              { printf("wrong input: \"%c\"\tbin: %d\n", yytext[0], yytext[0]); cleanup(-1); }

%%
