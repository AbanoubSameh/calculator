%{
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "functions.h"
#define YYSTYPE double
void cleanup(int);
int yylex();

void yyerror(const char *str) {
    printf("%s\n", str);
    cleanup(-1);
}

int yywrap() {
    return 1;
}

%}

%start exp
%token NUM END BIN HEX OPR CPR
%token ADD SUB MUL DIV MOD PI E
%token SIN COS TAN ASIN ACOS ATAN POW LOG LN
%token AND OR BAND BOR BXOR
%token EQU NEQ SEQ GEQ STH GTH NOT
%token SHR SHL ROR ROL TIL
%right POW

%%

postfix
    : OPR start CPR                     { $$ = $2; }
    | SIN start CPR                     { $$ = sin($2 * M_PI / 180); }
    | COS start CPR                     { $$ = cos($2 * M_PI / 180); }
    | TAN start CPR                     { $$ = tan($2 * M_PI / 180); }
    | ASIN start CPR                    { $$ = asin($2) * 180 / M_PI ; }
    | ACOS start CPR                    { $$ = acos($2) * 180 / M_PI ; }
    | ATAN start CPR                    { $$ = atan($2) * 180 / M_PI ; }
    | LOG NUM OPR start CPR             { $$ = log($4) / log($2); }
    | LOG OPR start CPR                 { $$ = log10($3); }
    | LN OPR start CPR                  { $$ = log($3); }
    | postfix POW postfix               { $$ = pow($1, $3); }
    | PI                                { $$ = M_PI; }
    | E                                 { $$ = M_E; }
    | NUM
    ;

unary
    : TIL unary                         { $$ = ~((int) $2); }
    | NOT unary                         { $$ = !$2; }
    | ADD unary                         { $$ = +$2; }
    | SUB unary                         { $$ = -$2; }
    | postfix
    ;

multiplicative
    : multiplicative MUL unary          { $$ = $1 * $3; }
    | multiplicative DIV unary          { $$ = $1 / $3; }
    | multiplicative MOD unary          { $$ = (int) $1 % (int) $3; }
    | unary
    ;

additive
    : additive ADD multiplicative       { $$ = $1 + $3; }
    | additive SUB multiplicative       { $$ = $1 - $3; }
    | multiplicative
    ;

shift
    : shift SHR additive                { $$ = (int) $1 >> (int) $3; }
    | shift SHL additive                { $$ = (int) $1 << (int) $3; }
    | shift ROR additive                { $$ = rotate_right((unsigned long) $1, (int) $3, sizeof((unsigned long) $1)); }
    | shift ROL additive                { $$ = rotate_left((unsigned long) $1, (int) $3, sizeof((unsigned long) $1)); }
    | additive
    ;

relational
    : relational STH shift              { if ($1 < $3) $$ = 1; else $$ = 0; }
    | relational GTH shift              { if ($1 > $3) $$ = 1; else $$ = 0; }
    | relational SEQ shift              { if ($1 <= $3) $$ = 1; else $$ = 0; }
    | relational GEQ shift              { if ($1 >= $3) $$ = 1; else $$ = 0; }
    | shift
    ;

equality
    : equality EQU relational           { if ($1 == $3) $$ = 1; else $$ = 0; }
    | equality NEQ relational           { if ($1 != $3) $$ = 1; else $$ = 0; }
    | relational
    ;

bitwise_and
    : bitwise_and BAND equality         { $$ = (int) $1 & (int) $3; }
    | equality
    ;

bitwise_xor
    : bitwise_xor BXOR bitwise_and      { $$ = (int) $1 ^ (int) $3; }
    | bitwise_and
    ;

bitwise_or
    : bitwise_or BOR bitwise_xor        { $$ = (int) $1 | (int) $3; }
    | bitwise_xor
    ;

and
    : and AND additive                  { if ($1 && $3) $$ = 1; else $$ = 0; }
    | bitwise_or
    ;

or
    : or OR and                         { if ($1 || $3) $$ = 1; else $$ = 0; }
    | and
    ;

start
    : or
    ;

exp
    : END                               { puts("empty, done");          return 0; }
    | start END                         { printf("answer: %.9g\n", $1); return 1; }
    | start BIN END                     { printf("answer in bin: "); print_bin((int) $1, sizeof((int) $1)); printf("\n"); return 1; }
    | start HEX END                     { printf("answer in hex: %x\n", (int) $1); return 1; }
    ;

%%
