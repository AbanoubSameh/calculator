
CC = gcc
CFLAGS = -Wall -Wextra
LDFLAGS = -lm

SRC = .
OBJ = obj
FILES = $(OBJ)/parser.o $(OBJ)/lexer.o $(OBJ)/main.o $(OBJ)/functions.o

front: $(FILES)
	$(CC) $(CFLAGS) $(LDFLAGS) -o front $(FILES)

$(SRC)/parser.c: grammer.y
	yacc -d -o $(SRC)/parser.c grammer.y

$(SRC)/lexer.c: tokens.l
	lex -o $(SRC)/lexer.c tokens.l

$(OBJ)/%.o: $(SRC)/%.c
	$(CC) $(CFLAGS) -o $@ -c $(SRC)/$<

clean:
	rm -f $(OBJ)/* $(SRC)/parser.c $(SRC)/parser.h $(SRC)/lexer.c front

rebuild: clean front
