
#ifndef _functions_h_
#define _functions_h_


void print_bin(unsigned long, int);
long rotate_left(unsigned long, int, int);
long rotate_right(unsigned long, int, int);

#endif
