#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curses.h>
#include "parser.h"
extern FILE* yyin;

FILE *f;
FILE *r;
const char *fileName = "new";

void init() {
    initscr();
    raw();
    keypad(stdscr, TRUE);
    noecho();
}

void cleanup(int exit_code) {
    system("sleep 3");
    fclose(f);
    remove(fileName);
    refresh();
    endwin();
    exit(exit_code);
}

void print_code(char *str) {
    for (int i = 0; i <= strlen(str); i++) {
        printf("%d\n", str[i]);
    }
}

int main() {
    int ch;
    init();
    r = fopen(fileName, "w");
    fflush(r);
    f = fopen(fileName, "r");
    yyin = f;

    char buffer[256];
    memset(buffer, 0, 256);
    int index = 0, response = 1;

    printw("enter an expression\n");
    while((ch = getch()) != 3 && response) {
        switch(ch) {
            case KEY_UP:
                printw("going up\n");
                break;
            case KEY_DOWN:
                printw("going down\n");
                break;
            case KEY_LEFT:
                printw("going right\n");
                break;
            case KEY_RIGHT:
                printw("going left\n");
                break;
            case 127:
                printw("%c%c%c", 8, 32, 8);
                if (index > 0) index--;
                buffer[index] = 0;
                break;
            case 10:
                //printf("\tyou entered: %s\n%c", buffer, 8);
                print_code(buffer);
                fprintf(r, "%s", buffer);
                fflush(r);
                memset(buffer, 0, 256);
                index = 0;
                response = yyparse();
                printw("enter an expression\n");
                break;
            default: {
                if (ch != 10) {
                    printw("%c", ch);
                    buffer[index] = (char) ch;
                    index++;
                }
            }
        }
    }
    cleanup(0);
}
