#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include "parser.h"
extern FILE* yyin;
FILE *f;
FILE *r;

void cleanup(int exit_int) {
    fclose(f);
    fclose(r);
    //remove("new");
    printf("\n");
    exit(exit_int);
}

void print_code(char *str) {
    for (int i = 0; i <= strlen(str); i++) {
        printf("%d\n", str[i]);
    }
}

int main() {
    signal(SIGINT, cleanup);
    r = fopen("new", "w");
    fflush(r);
    f = fopen("new", "r");
    yyin = f;
    char buffer[256];
    memset(buffer, 0, 256);
    char temp;
    int response = 1, index = 0;
    printf("enter an expression\n");
    while ((temp = getc(stdin)) != 3 && response) {
        if (temp == 10) {
            fprintf(r, "%s\n", buffer);
            fflush(r);
            print_code(buffer);
            response = yyparse();
            printf("you entered: %s\n", buffer);
            memset(buffer, 0, 256);
            index = 0;
            printf("enter an expression\n");
        } else if (temp == 127) {
            printf("%c%c%c", 8, 32, 8);
            if (index > 0) index--;
            buffer[index] = 0;
            break;
        }else {
            if (temp != 10) {
                //printf("%c", temp);
                buffer[index] = temp;
                index++;
            }
        }
    }
    printf("exiting...\n");
}
