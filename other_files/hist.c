#include <alloca.h>
#include <features.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int getNextLine(FILE *file) {
    int begin = ftell(file);
    char temp;
    for (int i = begin; 1; i++) {
        fseek(file, i, 0);
        temp = fgetc(file);
        if (temp == EOF) {
            return ftell(file) - begin;
        }
        if (temp == '\n') {
            return ftell(file) - begin;
        }
    }
}

int getPreviousLine(FILE *file) {
    fseek(file, ftell(file) - 2, 0);
    int begin = ftell(file);
    char temp;
    for (int i = begin; 1; i--) {
        fseek(file, i, 0);
        temp = fgetc(file);
        if (ftell(file) == 0) {
            fseek(file, SEEK_SET, 0);
            return ftell(file) - begin;
        }
        if (temp == '\n') {
            return ftell(file) - begin - 2;
        }
    }
}

int getLine(FILE* file, int lineCount) {
    int result = 0;
    if (lineCount > 0) {
        for (int i = 0; i < lineCount; i++) {
            result = getNextLine(file);
        }
    } else if (lineCount < 0) {
        lineCount = -lineCount;
        for (int i = 0; i < lineCount; i++) {
            result = getPreviousLine(file);
        }
    } else {
        int counter = 0;
        while ((counter = getNextLine(file)) != 0) {
            result += counter;
        }
    }
    return result;
}

int main() {
    FILE *file = fopen("hello", "r");
    char buffer[200];
    int x;
    printf("%d\n", feof(file));
    printf("\n");

    while ((x = getLine(file, 1)) != 0) {
        printf("%d\n", x);
    }
    x = 0;
    printf("\n");
    while ((x = getLine(file, -1)) != 0) {
        printf("%d\n", x);
    }
}
