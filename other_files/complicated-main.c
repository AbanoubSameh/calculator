#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curses.h>
#include "parser.h"
extern FILE* yyin;

FILE *f, *r, *s;
const char *fileName = "new";

void init() {
    initscr();
    raw();
    keypad(stdscr, TRUE);
    noecho();
}

void cleanup(int exit_code) {
    if (exit_code) {
        system("sleep 3");
    }
    fclose(f);
    remove(fileName);
    refresh();
    endwin();
    exit(exit_code);
}

void print_code(char *str) {
    for (int i = 0; i <= strlen(str); i++) {
        printf("%d\n", str[i]);
    }
}

int getNextLine(FILE *file) {
    int begin = ftell(file);
    char temp;
    for (int i = begin; 1; i++) {
        fseek(file, i, 0);
        temp = fgetc(file);
        if (temp == EOF) {
            fseek(file, 0, SEEK_END);
            return ftell(file) - begin;
        }
        if (temp == '\n') {
            return ftell(file) - begin;
        }
    }
}

int getPreviousLine(FILE *file) {
    fseek(file, ftell(file) - 2, 0);
    int begin = ftell(file);
    char temp;
    for (int i = begin; 1; i--) {
        fseek(file, i, 0);
        temp = fgetc(file);
        if (i == 0) {
            fseek(file, 0, SEEK_SET);
            return ftell(file) - begin;
        }
        if (temp == '\n') {
            return ftell(file) - begin - 2;
        }
    }
}

int getLine(FILE* file, int lineCount) {
    int result = 0;
    if (lineCount > 0) {
        for (int i = 0; i < lineCount; i++) {
            result = getNextLine(file);
        }
    } else if (lineCount < 0) {
        lineCount = -lineCount;
        for (int i = 0; i < lineCount; i++) {
            result = getPreviousLine(file);
        }
    } else {
        int counter = 0;
        while ((counter = getNextLine(file)) != 0) {
            result += counter;
        }
    }
    return result;
}

int main() {
    int ch;
    init();
    r = fopen(fileName, "w");
    fflush(r);
    f = fopen(fileName, "r");
    yyin = f;
    s = fopen(fileName, "r");

    int x;
    char seek[256];
    char temp[256];
    char *buffer;
    memset(seek, 0, 256);
    memset(temp, 0, 256);
    int index = 0, response = 1;

    printw("enter an expression\n");
    while((ch = getch()) != 3 && response) {
        switch(ch) {
            case KEY_UP:
                x = getLine(s, -1);
                if (x) {
                    fgets(seek, 256, s);
                    getLine(s, -1);
                    //printf("file pointer: %ld, text: %s%c\n", ftell(s), seek, 13);
                    seek[strlen(seek) - 1] = 0;
                    index = strlen(seek);
                    printw("%c%s", 13, seek);
                    buffer = seek;
                }
                break;
            case KEY_DOWN:
                if (fgets(seek, 256, s)) {
                    //printf("file pointer: %ld, text: %s%c\n", ftell(s), seek, 13);
                    seek[strlen(seek) - 1] = 0;
                    index = strlen(seek);
                    printw("%c%s", 13, seek);
                    buffer = seek;
                } else {
                    index = strlen(temp);
                    printw("%c%s\n", 13, temp);
                    buffer = temp;
                }
                break;
            case 127:
                printw("%c%c%c", 8, 32, 8);
                if (index > 0) index--;
                buffer[index] = 0;
                break;
            case 10:
                fprintf(r, "%s\n", buffer);
                fflush(r);
                getLine(s, 0);
                printf("file last: %ld\n", ftell(s));

                memset(buffer, 0, 256);
                index = 0;
                printf("\n%c", 13);
                response = yyparse();
                printw("%center an expression\n", 13);
                break;
            default: {
                if (ch != 10) {
                    printw("%c", ch);
                    buffer[index] = (char) ch;
                    index++;
                }
            }
        }
    }
    cleanup(0);
}
