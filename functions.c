#include <stdio.h>
#include <math.h>

void print_bin(unsigned long bin, int size) {
    int size_in_bits = size * 8;
    unsigned long temp;
    for (int i = size_in_bits - 1; i >= 0; i--) {
        temp = bin & ((unsigned long) pow(2, i));
        temp >>= i;
        printf("%lu", temp);
    }
}

long rotate_left(unsigned long number, int to_rotate, int size) {
    int size_in_bits = size * 8;
    unsigned long to_copy;
    unsigned long temp;
    unsigned long to_return = number;
    if (to_rotate > size_in_bits) {
        to_rotate = to_rotate % size_in_bits;
    }
    if (to_rotate > 0) {
        to_copy = (unsigned long) pow(2, to_rotate) - 1;
        to_copy = to_copy << (size_in_bits - to_rotate);
        temp = number & to_copy;
        temp = temp >> (size_in_bits - to_rotate);
        to_return = to_return << to_rotate;
        to_return = to_return | temp;
    }
    return to_return;
}

long rotate_right(unsigned long number, int to_rotate, int size) {
    int size_in_bits = size * 8;
    unsigned long to_copy;
    unsigned long temp;
    unsigned long to_return = number;
    if (to_rotate > size_in_bits) {
        to_rotate = to_rotate % size_in_bits;
    }
    if (to_rotate > 0) {
        to_copy = (unsigned long) pow(2, to_rotate) - 1;
        temp = number & to_copy;
        temp = temp << (size_in_bits - to_rotate);
        to_return = to_return >> to_rotate;
        to_copy = (unsigned long) pow(2, size_in_bits - to_rotate) - 1;
        to_return = to_return & to_copy;
        to_return = to_return | temp;
    }
    return to_return;
}
